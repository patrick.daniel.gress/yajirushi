from yajirushi import arrow

add_min_and_max = arrow(max) & arrow(min) | arrow(sum)
assert add_min_and_max([1, 2, 3]) == 1 + 3
