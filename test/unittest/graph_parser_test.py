from toolz import identity, first

from yajirushi.arrows.default import arrow
from yajirushi.description.flowgraph import parse_graph, FlowGraph


def test_lift_parsing():
    graph: FlowGraph = parse_graph(arrow(identity))
    assert len(graph.nodes) == 1
    assert first(graph.nodes.values()) == identity


def test_composition_parsing():
    my_arrow = arrow(identity) >> arrow(max)
    graph: FlowGraph = parse_graph(my_arrow)
    print(graph)


def test_clone_parsing():
    my_arrow = arrow(min) & arrow(max)
    graph: FlowGraph = parse_graph(my_arrow)
    assert graph == FlowGraph(nodes={0: max, 1: min, 2: 'split'}, edges={(2, 0), (2, 1)})


def test_merge_parsing():
    my_arrow = (arrow(min) & arrow(max)) | arrow(sum)
    graph: FlowGraph = parse_graph(my_arrow)
    print(graph)