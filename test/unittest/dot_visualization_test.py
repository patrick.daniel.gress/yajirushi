from re import sub

from yajirushi import arrow_to_dot, identity_arrow

IDENTITY_ARROW_DOT: str = """digraph arrow { N0[label="identity"] }"""


def _normalize_dot_output(dot: str) -> str:
    return sub(r'\s+', ' ', dot).strip()


def test_simple_dot_visualization():
    assert _normalize_dot_output(arrow_to_dot(identity_arrow())) == IDENTITY_ARROW_DOT
