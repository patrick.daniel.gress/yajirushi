from yajirushi import star_arrow, identity_arrow
from yajirushi.arrows.default import Arrow, line, merge, first, compose, second, split, arrow


def square(x: int) -> int: return x*x
def inc(x: int) -> int: return x+1
def as_fraction(value: tuple[int, int]) -> float: return value[0] / value[1]
def invert_fraction(value: float) -> float: return 1.0 / value


def test_complex_arrow():
    lined = line(Arrow(square), Arrow(inc))
    assert lined(5) == (25, 6)
    merged = merge(lined, Arrow(as_fraction))
    assert merged(3) == 2.25


def test_first_second():
    square_lower = second(Arrow(square))
    inc_upper = first(Arrow(inc))
    assert square_lower((3, 2)) == (3, 4)
    assert inc_upper((3, 2)) == (4, 2)


def test_full_pipeline():
    pipeline = merge(
        compose(compose(split(Arrow(square)), first(Arrow(square))), second(Arrow(inc))),
        Arrow(as_fraction)
    )
    assert pipeline(2) == 16/5


def test_combine_operator():
    pipeline = Arrow(square) * Arrow(inc)
    assert pipeline((2, 2)) == (4, 3)


def test_compose_operator():
    pipeline = Arrow(square) >> Arrow(inc)
    assert pipeline(2) == 5


def test_type_deduction():
    pipeline = Arrow(square) >> Arrow(str)


def test_and_operator():
    pipeline = Arrow(square) & Arrow(inc)
    assert pipeline(2) == (4, 3)


def test_operator_pipeline():
    pipeline = arrow(square) & arrow(inc) | arrow(as_fraction) + arrow(invert_fraction)
    assert pipeline(2) == 3/4
    pipeline = arrow(square) & arrow(inc) | arrow(as_fraction) >> arrow(invert_fraction)
    assert pipeline(2) == 3/4


def test_arrow_lift():
    inc_arrow = arrow(inc)
    assert inc_arrow.func == inc
    assert star_arrow(lambda x, y: x + y)([2, 1]) == 3
    assert identity_arrow()(42) == 42