API Documentation
=================


.. autoclass:: yajirushi.Arrow
   :special-members: __rshift__, __mul__, __and__, __or__