Auth
----




.. code-block:: python

     from dataclasses import dataclass
     from datetime import datetime, timedelta
     
     from yajirushi import arrow, star_arrow
     
     
     @dataclass
     class User:
         username: str
     
     
     @dataclass
     class Auth(User):
         password: str
     
     
     
     
     @dataclass
     class Permission:
         user: str
         until: datetime
     
     
     
     users: list[Auth] = [
         Auth('alice', 'secret'),
         Auth('bob', 'top-secret')
     ]
     
     
     def log_auth_attempt(user: Auth) -> datetime:
         print(user)
         return datetime.now()
     
     
     def check_credentials(user: Auth) -> User | None:
         return User(user.username) if user in users else None
     
     
     def grant_access(
         user: User | None,
         start: datetime
     ) -> Permission | None:
         return Permission(
             user.username,
             start + timedelta(hours=1)
         ) if user else None
     
     
     log_and_check = arrow(check_credentials)\
                     & arrow(log_auth_attempt) \
                     | star_arrow(grant_access)
     
     assert isinstance(log_and_check(users[0]), Permission)
     assert log_and_check(Auth('charile', 'random')) is None
     
     
     
     





log_and_check
~~~~~~~~~~~~~

.. graphviz::

    digraph log_and_check {
    	N0[label="log_auth_attempt"]
    	N1[label="check_credentials"]
    	N2[label="split"]
    	N4[label="grant_access"]
    	N2 -> N0
    	N2 -> N1
    	N1 -> N4
    	N0 -> N4
    }

