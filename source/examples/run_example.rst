Run example
-----------




.. code-block:: python

     from dataclasses import dataclass
     from importlib.util import spec_from_file_location, module_from_spec
     import sys
     from os import listdir
     from pathlib import Path
     from types import ModuleType
     
     from jinja2 import Template
     
     from yajirushi import Arrow, arrow_to_dot
     
     
     def local_file(
         name: str,
         extension: str = 'py'
     ) -> Path:
         return Path(__file__).parent.joinpath(f'{name}.{extension}')
     
     
     def get_example_module(example_name: str) -> ModuleType:
         spec = spec_from_file_location(example_name, local_file(example_name))
         example_module = module_from_spec(spec)
         sys.modules[example_name] = example_module
         spec.loader.exec_module(example_module)
         return example_module
     
     
     def find_arrows_in_module(module: ModuleType) -> dict[str, Arrow]:
         return dict(
             (entity_name, entity)
             for entity_name in dir(module)
             if isinstance((entity := getattr(module, entity_name)), Arrow)
         )
     
     
     @dataclass
     class Example:
         name: str
         code: str
         graphs: dict[str, str]
     
     
     def create_example_for_doc(example_name: str) -> Example:
         with local_file(example_name).open() as src:
             code = ''.join(f'     {line}' for line in src)
         return Example(
             example_name,
             code,
             dict(
                 (name, arrow_to_dot(arrow, graph_name=name))
                 for (name, arrow) in find_arrows_in_module(get_example_module(example_name)).items()
             )
         )
     
     
     def load_template(name: str) -> Template:
         with local_file(name, 'jinja').open(encoding='utf-8') as src:
             return Template(src.read())
     
     
     def indent(content: str, size: int) -> str:
         return '\n'.join(f'{" "*size}{line}' for line in content.split('\n'))
     
     
     def to_sphinx(example: Example):
         headline = example.name.capitalize().replace('_', ' ')
         with Path(__file__).parent.parent.joinpath('source', 'examples', f'{example.name}.rst').open('w') as out:
             out.write(
                 load_template('sphinx').render(
                     headline=f'{headline}\n{"-"*len(headline)}\n\n',
                     code=example.code,
                     graphs=example.graphs,
                     indent=indent
                 )
             )
     
     
     if __name__ == '__main__':
         for file in listdir(Path(__file__).parent):
             if file.endswith('.py'):
                 to_sphinx(create_example_for_doc(file[0:-3]))



