Add min and max
---------------




.. code-block:: python

     from yajirushi import arrow
     
     add_min_and_max = arrow(max) & arrow(min) | arrow(sum)
     assert add_min_and_max([1, 2, 3]) == 1 + 3





add_min_and_max
~~~~~~~~~~~~~~~

.. graphviz::

    digraph add_min_and_max {
    	N0[label="min"]
    	N1[label="max"]
    	N2[label="split"]
    	N4[label="sum"]
    	N2 -> N0
    	N2 -> N1
    	N1 -> N4
    	N0 -> N4
    }

