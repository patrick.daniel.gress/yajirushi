from collections.abc import Sequence
from dataclasses import dataclass, field
from enum import Enum
from typing import Self, Callable, Any, TypeVar

S_ = TypeVar('S_')


class Operators(Enum):
    COMPOSE = '>>>'
    CLONE = '&&&'
    MERGE = '|'
    LIFT = 'arrow'
    RAIL = '***'


@dataclass(frozen=True)
class StreamOperation:
    name: str
    args: Sequence[Self | Callable[[Any], Any]] = field(default_factory=tuple)


