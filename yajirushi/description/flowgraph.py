from dataclasses import dataclass, field
from functools import reduce, partial
from itertools import count, chain, product
from operator import itemgetter, add, contains
from typing import Callable, Any, Iterable, Self

from toolz import complement

from yajirushi.arrows.default import Arrow
from yajirushi.description.stream import StreamOperation, Operators


@dataclass
class FlowGraph:
    nodes: dict[int, Any] = field(default_factory=dict)
    edges: set[tuple[int, int]] = field(default_factory=set)

    @property
    def end_node(self) -> int:
        return min(self.nodes)

    @property
    def root_node(self) -> int:
        return max(self.nodes)

    def __pow__(self, power: int, modulo=None) -> Self:
        return FlowGraph(
            nodes=dict((k+power, v) for (k, v) in self.nodes.items()),
            edges={(s+power, d+power) for (s, d) in self.edges}
        )

    def __add__(self, right: Self) -> Self:
        return FlowGraph(
            dict(chain(self.nodes.items(), right.nodes.items())),
            self.edges.union(right.edges).union({(self.end_node, right.root_node)})
        )


def _combine(lower: FlowGraph, upper: FlowGraph) -> FlowGraph:
    split_node: int = max(lower.root_node, upper.root_node) + 1
    return FlowGraph(
        dict(chain(lower.nodes.items(), upper.nodes.items(), [(split_node, 'split')])),
        lower.edges.union(upper.edges).union({(split_node, lower.root_node), (split_node, upper.root_node)})
    )


def _merge(left: FlowGraph, right: FlowGraph) -> FlowGraph:
    return FlowGraph(dict(chain(left.nodes.items(), right.nodes.items())), left.edges.union(right.edges))


def _ends(graph: FlowGraph) -> set[int]:
    starts: set[str] = set(map(itemgetter(0), graph.edges))
    return set(filter(complement(partial(contains, starts)), graph.nodes))


def _starts(graph: FlowGraph) -> set[int]:
    ends: set[str] = set(map(itemgetter(1), graph.edges))
    return set(filter(complement(partial(contains, ends)), graph.nodes))


def parse_graph(arrow_: Arrow | StreamOperation, *, node_names: Iterable[int] | None = None) -> FlowGraph:
    """
    Extracts a visual representation of an Arrow in form of a graph.
    Args:
        arrow_: Arrow to be parsed.
        node_names: Iterable of ints for numbering the graphs, nodes. Default is natural numbers.

    Returns:
        FlowGraph: Graph representing the flow of the arrow operations.
    """
    if isinstance(arrow_, Arrow):
        return parse_graph(arrow_.origin, node_names=node_names)
    node_names = node_names if node_names is not None else count()
    parse: Callable[[StreamOperation], FlowGraph] = partial(parse_graph, node_names=node_names)
    match arrow_:
        case StreamOperation(name=Operators.LIFT.value, args=(func,)):
            return FlowGraph({next(node_names): func})
        case StreamOperation(name=Operators.COMPOSE.value, args=(start, *rest)):
            return reduce(add, map(partial(parse), rest), parse(start))
        case StreamOperation(name=Operators.CLONE.value, args=(upper, lower)):
            return _combine(parse(lower), parse(upper))
        case StreamOperation(name=Operators.RAIL.value, args=(upper, lower)):
            return _combine(parse(lower), parse(upper))
        case StreamOperation(name=Operators.MERGE.value, args=(stream, by)):
            stream_graph: FlowGraph = parse(stream)
            merge_graph: FlowGraph = parse(by) ** (len(stream_graph.nodes) - 1)
            graph: FlowGraph = _merge(stream_graph, merge_graph)
            graph.edges.update(product(_ends(stream_graph), _starts(merge_graph)))
            return graph
        case None:
            raise AttributeError(f'Arrow {arrow_} has not origin information for parsing.')

