from typing import TypeVar, Generic, Callable

from toolz import compose as compose_function, identity

from yajirushi.description.stream import StreamOperation

P_ = TypeVar('P_')
Q_ = TypeVar('Q_')
R_ = TypeVar('R_')
S_ = TypeVar('S_')
T_ = TypeVar('T_')


class Arrow(Generic[P_, R_]):
    """An Arrow is a functional abstraction for composing and manipulating functions.

    Arrows represent functions and provide composition operators to connect them
    seamlessly. This library allows you to build data processing pipelines in a
    declarative and composable manner.

    Args:
        func (Callable[[P_], R_]): The underlying function that this Arrow represents.
        origin (StreamOperation | None, optional): The origin of this Arrow in a stream operation.
            Defaults to None.

    Attributes:
        func (Callable[[P_], R_]): The underlying function represented by this Arrow.
        origin (StreamOperation | None): The origin of this Arrow in a stream operation.

    """
    def __init__(self, func: Callable[[P_], R_], *, origin: StreamOperation | None = None) -> None:
        self.func: Callable[[P_], R_] = func
        self.origin: StreamOperation | None = origin

    def __call__(self, value: P_) -> R_:
        """Invoke the Arrow by applying its function to a value.

        Args:
            value (P_): The input value to be processed.

        Returns:
            R_: The result of applying the Arrow's function to the input value.
        """
        return self.func(value)

    def __mul__(self, other: 'Arrow[Q_, S_]') -> 'Arrow[tuple[P_, Q_], tuple[R_, S_]]':
        """Compose this Arrow with another Arrow using the 'rail' operation.

        Args:
            other (Arrow[Q_, S_]): The other Arrow to compose with.

        Returns:
            Arrow[tuple[P_, Q_], tuple[R_, S_]]: A new Arrow resulting from composing this and the other Arrow.
        """
        return rail(self, other)

    def __rshift__(self, other: 'Arrow[R_, S_]') -> 'Arrow[P_, S_]':
        """
        Compose two arrows by applying them in a serial way like function composition.
        Args:
            other: First arrow in the line of composition.

        Returns:
            Arrow[T_, R_]: A new Arrow resulting from composing this and the other Arrow.

        """
        return compose(self, other)

    def __add__(self, other: 'Arrow[R_, T_]') -> 'Arrow[P_, T_]':
        return compose(self, other)

    def __and__(self, other: 'Arrow[P_, S_]') -> 'Arrow[P_, tuple[R_, S_]]':
        return clone(self, other)

    def __or__(self, other: 'Arrow[R_, S_]') -> 'Arrow[P_, S_]':
        return compose(self, other, op_name='|')


def arrow(func: Callable[[P_], R_]) -> Arrow[P_, R_]:
    """Create an Arrow from a given function.

    Args:
        func (Callable[[P_], R_]): The function to create an Arrow from.

    Returns:
        Arrow[P_, R_]: An Arrow representing the input function.
    """
    return Arrow(func, origin=StreamOperation('arrow', (func,)))


def compose(inner: Arrow[P_, R_], outer: Arrow[R_, T_], *, op_name: str = '>>>') -> Arrow[P_, T_]:
    return Arrow(
        compose_function(outer.func, inner.func),
        origin=StreamOperation(op_name, (inner.origin, outer.origin))
    )


def _split(func: Callable[[P_], R_]) -> Callable[[P_], tuple[R_, R_]]:
    def split_func(value: P_) -> tuple[R_, R_]:
        return (result := func(value)), result
    return split_func


def split(arrow_: Arrow[P_, R_]) -> Arrow[P_, tuple[R_, R_]]:
    """Create an Arrow that splits its input by applying the input Arrow's function twice.

    Args:
        arrow_ (Arrow[P_, R_]): The input Arrow to be split.

    Returns:
        Arrow[P_, tuple[R_, R_]]: An Arrow that splits the input value into a tuple of two results.
    """
    return Arrow(_split(arrow_.func), origin=StreamOperation('split', (arrow_.origin,)))


def line(upper: Arrow[P_, R_], lower: Arrow[P_, T_]) -> Arrow[P_, tuple[R_, T_]]:
    """Create an Arrow that applies two Arrows to its input and returns a tuple of results.

    Args:
        upper (Arrow[P_, R_]): The upper Arrow.
        lower (Arrow[P_, T_]): The lower Arrow.

    Returns:
        Arrow[P_, tuple[R_, T_]]: An Arrow that applies both input Arrows and returns a tuple of results.
    """
    def line_func(value: P_) -> tuple[R_, T_]:
        return upper.func(value), lower.func(value)

    return Arrow(line_func, origin=StreamOperation('line', (upper.origin, lower.origin)))


def merge(arrow_: Arrow[P_, tuple[R_, T_]], by: Arrow[tuple[R_, T_], S_]) -> Arrow[P_, S_]:
    """Create an Arrow that merges the results of two Arrows using a given merging Arrow.

    Args:
        arrow_ (Arrow[P_, tuple[R_, T_]]): The Arrow whose results are to be merged.
        by (Arrow[tuple[R_, T_], S_]): The merging Arrow.

    Returns:
        Arrow[P_, S_]: An Arrow that merges the results of the input Arrow and the merging Arrow.
    """
    return Arrow(compose_function(by.func, arrow_.func), origin=StreamOperation('|', (arrow_.origin, by.origin)))


def first(arrow_: Arrow[P_, R_]) -> Arrow[tuple[P_, Q_], tuple[R_, Q_]]:
    """Create an Arrow that applies the input Arrow and leaves the second element of the input tuple unchanged.

    Args:
        arrow_ (Arrow[P_, R_]): The input Arrow.

    Returns:
        Arrow[tuple[P_, Q_], tuple[R_, Q_]]: An Arrow that applies the input Arrow and leaves the second element unchanged.
    """
    return rail(arrow_, arrow(identity))


def second(arrow_: Arrow[P_, R_]) -> Arrow[tuple[Q_, P_], tuple[Q_, R_]]:
    """Create an Arrow that applies the input Arrow and leaves the first element of the input tuple unchanged.

    Args:
        arrow_ (Arrow[P_, R_]): The input Arrow.

    Returns:
        Arrow[tuple[Q_, P_], tuple[Q_, R_]]: An Arrow that applies the input Arrow and leaves the first element unchanged.
    """
    return rail(arrow(identity), arrow_)


def rail(upper: Arrow[P_, R_], lower: Arrow[Q_, S_]) -> Arrow[tuple[P_, Q_], [R_, S_]]:
    """Create an Arrow that applies two Arrows to a tuple of inputs and returns a tuple of results.

    Args:
        upper (Arrow[P_, R_]): The upper Arrow.
        lower (Arrow[Q_, S_]): The lower Arrow.

    Returns:
        Arrow[tuple[P_, Q_], [R_, S_]]: An Arrow that applies both input Arrows to a tuple of inputs and returns a tuple of results.
    """
    def railed_func(value: tuple[P_, Q_]) -> tuple[R_, S_]:
        return upper.func(value[0]), lower.func(value[1])
    return Arrow(railed_func, origin=StreamOperation('***', (upper.origin, lower.origin)))


def clone(upper: Arrow[P_, R_], lower: Arrow[P_, S_]) -> Arrow[P_, tuple[R_, S_]]:
    """Create an Arrow that applies two Arrows to the same input and returns a tuple of results.

    Args:
        upper (Arrow[P_, R_]): The upper Arrow.
        lower (Arrow[P_, S_]): The lower Arrow.

    Returns:
        Arrow[P_, tuple[R_, S_]]: An Arrow that applies both input Arrows to the same input and returns a tuple of results.
    """
    def cloned_func(value: P_) -> tuple[R_, S_]:
        return upper.func(value), lower.func(value)
    return Arrow(cloned_func, origin=StreamOperation('&&&', (upper.origin, lower.origin)))
