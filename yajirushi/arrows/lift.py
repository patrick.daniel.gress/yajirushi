from functools import wraps
from typing import TypeVar, ParamSpec, Callable, cast

from toolz import identity

from yajirushi import Arrow, arrow

P_ = ParamSpec('P_')
R_ = TypeVar('R_')


def star_arrow(func: Callable[P_, R_]) -> Arrow[P_, R_]:
    @wraps(func)
    def star(value: P_) -> R_:
        return func(*value)
    return arrow(star)


def identity_arrow() -> Arrow[R_, R_]:
    """
    Lifts the identity function to an arrow.

    Returns:
        Arrow that returns its value as result.
    """
    return cast(Arrow[R_, R_], arrow(identity))