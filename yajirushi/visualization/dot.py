from typing import Any, Callable

from yajirushi.arrows.default import Arrow
from yajirushi.description.flowgraph import FlowGraph, parse_graph


def _format_edge(edge: tuple[int, int]) -> str:
    return f'\tN{edge[0]} -> N{edge[1]}'


def _format_node(node: tuple[int, str | Callable[[Any], Any]]) -> str:

    label: str = node[1] if not hasattr(node[1], '__name__') else node[1].__name__
    return f'\tN{node[0]}[label="{label}"]'


def _to_dot(
    graph: FlowGraph,
    *,
    name: str = 'arrow',
    format_edge: Callable[[tuple[int, int]], str] | None = None,
    format_node: Callable[[tuple[int, str | Callable[[Any], Any]]], str] | None = None
) -> str:
    format_edge = format_edge if format_edge else _format_edge
    format_node = format_node if format_node else _format_node
    return '\n'.join(
        (
            f'digraph {name} {{', *map(format_node, graph.nodes.items()), *map(format_edge, graph.edges), '}'
        )
    )


def arrow_to_dot(
        arrow: Arrow,
        *,
        graph_name: str = 'arrow',
        format_edge: Callable[[tuple[int, int]], str] | None = None,
        format_node: Callable[[tuple[int, str | Callable[[Any], Any]]], str] | None = None
) -> str:
    return _to_dot(parse_graph(arrow), name=graph_name, format_edge=format_edge, format_node=format_node)