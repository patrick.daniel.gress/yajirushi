# Yajirushi

Yajirushi is a small library that enables the workflow associated with
[Arrows](https://en.wikibooks.org/wiki/Haskell/Understanding_arrows).

````python
from yajirushi import arrow

add_min_and_max = arrow(max) & arrow(min) | arrow(sum)
assert add_min_and_max([1, 2, 3]) == 1 + 3
````
